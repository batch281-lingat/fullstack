import React from 'react'

// Create a context object
// A context object as the name states is a data type of an object than can be used to store information that can be shared  within the app.
// The context object is a different approach to passing information between components and allows easier access avoiding the use of props

// using the createContext from react we can create a context in our app
// we contain the context created in our UserContext variable
// we named it Usercontext simply because this context will contain the information of our user.

const UserContext = React.createContext();

// The provider component allows other components to consume/use the context object and spply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;