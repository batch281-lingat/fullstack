import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import {Link} from 'react-router-dom'

/*export default function CourseCard({courseProp}) {
  console.log(courseProp)

  return (
    <Card>
        <Card.Body>
            <Card.Title>{courseProp.name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{courseProp.description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{courseProp.price}</Card.Text>
            <Button variant="primary">Enroll</Button>
        </Card.Body>
    </Card>
      
  )
}*/

export default function CourseCard({courseProp}) {
  // Checks to see if the data was successfully pased
  // console.log(courseProp);
  
  const {_id, name, description, price} = courseProp;

  // Use the state hook for this component to be able to store its state
  // States are used to keep track of information related to an individual components
  const [count, setCount] = useState(0)

  const [seats, setSeats] = useState(30)

  // We are going to create a new state that will declare or tell the value of the disabled property in the button
  const [isDisabled, setIsDisabled] = useState(false)

  // Function that keeps track of the enrollees
  //  The setter function for UseState are asynchronous allowing it to execute separately from other codes in the program
  //  The "setCount" function is b eing executed while the "console.log" is already completed
  function enroll() {
    if(seats > 1){
        setCount(count + 1)
        setSeats(seats - 1 )
        // console.log('Enrollees: ' + count)
    }else{
        alert("Congratulations on getting the last slot!");
        setCount(count + 1)
        setSeats(seats - 1)
    }
  }
  // Define a 'useEffect' hook to have 'CourseCard' component do or perform a certain task after every changes in the seats state
  // The side effect will run automatically in initial rendering and in evvery changes of the seats state.
  // The array in the useEffectt is called the dependency array
  useEffect(()=> {
        if(seats === 0){
            setIsDisabled(true)
        }

  }, [seats])

  return (
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Subtitle>Enrollees</Card.Subtitle>
            <Card.Text>{count} Enrollees</Card.Text>
            <Card.Subtitle>Seats</Card.Subtitle>
            <Card.Text>{seats}</Card.Text>
            {/*<Button variant="primary" onClick ={enroll} disabled={isDisabled}>Enroll</Button>*/}
             <Button as= {Link} to ={`/courses/${_id}`} variant="primary" disabled={isDisabled}>See more details</Button>
        </Card.Body>
    </Card>
  )
}

// Check if the CourseCard component is getting correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
CourseCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific shape.
    courseProp : PropTypes.shape({
        name : PropTypes.string.isRequired,
        description : PropTypes.string.isRequired,
        price : PropTypes.number.isRequired,
    })
}
