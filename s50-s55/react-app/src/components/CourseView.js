import { Col, Row, Button, Card} from 'react-bootstrap'

// the useParams allows us to get our extract the parameter included in our pages
import {useParams} from 'react-router-dom'
import {useEffect, useState} from  'react'

import Swal2 from 'sweetalert2'

export default function CourseView() {

	const [name, setName] = useState('')
	const [price, setPrice] = useState('')
	const [description, setDescription] = useState('')

	const {id} = useParams()
	/*console.log(id)*/
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/${id}`)
		.then(response => response.json())
		.then(data =>{
			// console.log(data)
			setName(data.name)
			setPrice(data.price)
			setDescription(data.description)
		})

	},[])

	const enroll = (courseId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method : "POST",
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
				courseId: `${courseId}`
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === true){
				Swal2.fire({
				title : 'Enrollment successful!',
				icon : 'success',
				text : 'You are now enrolled in the course!'
				})
			}else{
				Swal2.fire({
				title : 'Enrollment unsuccessful!',
				icon : 'error',
				text : 'Please try again!'
				})
			}
		})
	}



	return(
		<Row>
			<Col>
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Text>
				          {description}
				        </Card.Text>
				        <Card.Text>
				          Price : {price}
				        </Card.Text>
				        <Button variant="primary" onClick = {() => enroll(id)}>Enroll</Button>
				      </Card.Body>
				   </Card>
			</Col>
		</Row>
	)
}