import { Button, Form, Row, Col } from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import { Navigate, useNavigate } from 'react-router-dom'

//sweetalert2 is a simple and useful package for generating user alerts with reactJS
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext'

export default function Login(){

	const navigate = useNavigate()

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)

	// Allows us to consume the UserContext object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext)
	
	// const [user, setUser] = useState(localStorage.getItem('email'))

	useEffect(()=>{
		if(email !== ''  && password !== '' && email.length <= 15){
			setIsDisabled(false)
		}else {
			setIsDisabled(true)
		}
	}, [email, password])


	function loginUser(event){

		event.preventDefault()

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('property', value)
		// The localStorage.setItem() allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering.
		// Because REACT JS is a single page application, using the localStorage will not trigger rerendering of component.
		// localStorage.setItem('email', email)

		// setUser(localStorage.getItem('email'))

		// alert('You are now logged in!')
		// navigate('/courses')
		// setEmail('')
		// setPassword('')

		// Process a fetch request tot the corresponding backend API
		// Syntax:
			// fetch('url', {options}).then(response => response.json()).then(data =>{})
			//The environment variables are important for hiding sensitive pieces of information like the backend API URL which can be exploited if added directly into our code.
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
				method : 'POST',
				headers : {
					'Content-Type' : 'application/json'
				},
				body : JSON.stringify({
					email : email,
					password : password
				})
			})
		.then(response => response.json())
		.then(data => {
			// console.log(data)

			// If statement to check whether the login is successful or not.

			if (data === false){
				/*alert('Login unsuccessful!')*/
				// In adding sweetalert2 you have to use the fire method.
				Swal2.fire({
					position: 'top',
					title : "Login unsuccessful!",
					icon :'error',
					text: 'Check your login credentials and try again'
				})

			}else{
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)
				/*alert('Login Successful!')*/

				Swal2.fire({
					position: 'top',
					title:"Login successful!",
					icon: 'success',
					text: 'Welcome to Zuitt Booking!'
				})
				navigate('/courses')
			}
		
		})

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers : {
				Authorization : `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data =>{
			setUser({
				id: data._id,
				isAdmin : data.isAdmin
		})

		})

	}



return (
	user.id === null || user.id === undefined
		?
 		<Row>
 			<Col className = "col-5 mx-auto">
 				<h1 className = "text-center">Login</h1>
 			 	<Form onSubmit = {event => loginUser(event)}>
 			 	      <Form.Group className="mb-3" controlId="formBasicEmail">
 			 	        <Form.Label>Email address</Form.Label>
 			 	        <Form.Control 
 			 	        	type="email" 
 			 	        	placeholder="Enter email" 
 			 	        	value = {email}
 			 	        	onChange ={event => setEmail(event.target.value)}
 			 	        	 />
 			 	      </Form.Group>

 			 	      <Form.Group className="mb-3" controlId="formBasicPassword">
 			 	        <Form.Label>Password</Form.Label>
 			 	        <Form.Control 
 			 	        	type="password" 
 			 	        	placeholder="Password"
 			 	        	value = {password}
 			 	        	onChange ={event => setPassword(event.target.value)} />
 			 	      </Form.Group>
 			 	      <Button variant="success" type="submit" disabled = {isDisabled}>
 			 	        Login
 			 	      </Button>
 			 	 </Form>
 			</Col>
 		</Row>
 		:
 		<Navigate to = '/*' />
	)
}