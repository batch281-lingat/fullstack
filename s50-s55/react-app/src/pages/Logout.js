// It will allow us to navigate from one page of our application to another page
import {Navigate} from 'react-router-dom'

import {useContext, useEffect} from 'react'
import UserContext from '../UserContext'

export default function Logout(){
	// localStorage.clear()
	const {unsetUser, setUser} = useContext(UserContext)

	useEffect(()=> {
		// the value from our localStorage will be cleared
		unsetUser()
		// Since we cleared the contents of our local storage using the unsetUser() therefore the value of our user state will be null
		// by adding useEffect, this will allow the Logout page to render first before triggering the useEffectt which changes the state of our user
		setUser({
			id: null,
			isAdmin : null
		})
	},[])

	return (
		<Navigate to = '/login'/>
	)

}