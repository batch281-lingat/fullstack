import { Button, Form, Row, Col } from 'react-bootstrap'
// 1 We need to import the useState from the react
import {useState, useEffect , useContext } from 'react'

import { Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'

import Swal2 from 'sweetalert2'

export default function Register(){

	const navigate = useNavigate()
	const {user} = useContext(UserContext)

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	const [isPassed, setIsPassed] = useState(true)
	const [numIsPassed, setNumIsPassed] = useState(true)
	const [isDisabled, setIsDisabled] = useState(true)

	// We are going to add/create a state tahtt will declare whether the password 1 and password 2 is equal
	const [isPasswordMatch , setIsPasswordMatch] = useState(true)

	// When the email changes it will have a side effect that will console its value
	useEffect(()=>{
		if(email.length > 15){
			setIsPassed(false)
		}else{
			setIsPassed(true)
		}
	}, [email]);
	// This useEffect will disable or enable our sign up button
	useEffect(()=>{
		// we are going to add if statement and all the condition that we mention should be statisfied
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 &&email.length <= 15 && firstName !== '' && lastName !== '' && mobileNo.length >= 11 ){
			setIsDisabled(false);
		}else{
			setIsDisabled(true)
		}
	}, [email, password1, password2, firstName, lastName, mobileNo])
	// Function to simulate user Registration
	function registerUser(event){
		// prevent page reloading
		event.preventDefault()
		// ACTIVITY s55
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
				method: 'POST',
				headers: { 'Content-Type' : 'application/json' },
				body: JSON.stringify({
					email: email
				})
			})
		.then(response => response.json())
		.then(data => {
			if (data === false){
					fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
						method : 'POST',
						headers : {
							'Content-Type': 'application/json'
						},
						body : JSON.stringify({
							email: email,
							password: password1,
							firstName : firstName,
							lastName: lastName,
							mobileNo: mobileNo
						})
					})
					.then(response => response.json())
					.then(data =>{
						if(data === true){
							Swal2.fire({
								title: 'Registration successful!',
								alert: 'success',
								text : 'Congratulations on registering!'
							})
							navigate('/login')
						}else{
							Swal2.fire({
								title : 'Registration failed!',
								icon : 'error',
								text : 'Please try again!'
							})
						}
					})
			}else {
				Swal2.fire({
					title : 'User Exists',
					icon : 'error',
					text : 'Duplicate Email Found!'
				})
			}
		})
	}


	useEffect(()=>{
		if(mobileNo.length > 11){
			setNumIsPassed(false)
		}else{
			setNumIsPassed(true)
		}
	}, [mobileNo]);

	// useEffect to validate whether the password1 is equal to password2
	useEffect(() =>{
		if(password1 === password2){
			setIsPasswordMatch(true)
		}else{
			setIsPasswordMatch(false)
		}
	}, [password1, password2])

	return(
  		user.id === null || user.id === undefined
  		?
  		<Row>
  			<Col className = "col-6 mx-auto">
  				<h1 className = "text-center">Register</h1>
  				<Form onSubmit = {event => registerUser(event)}>
  				   	
  				   	<Form.Group className="mb-3" controlId="formFirstName">
  				   	  <Form.Label>First Name</Form.Label>
  				   	  <Form.Control 
  				   	  		type="text" 
  				   	  		placeholder="Enter First Name"
  				   	  		value ={firstName}
  				   	  		onChange ={event => setFirstName(event.target.value)}
  				   	  		/>
  				   	</Form.Group>
  				   	<Form.Group className="mb-3" controlId="formLastName">
  				   	  <Form.Label>Last Name</Form.Label>
  				   	  <Form.Control 
  				   	  		type="text" 
  				   	  		placeholder="Enter Last Name"
  				   	  		value ={lastName}
  				   	  		onChange ={event => setLastName(event.target.value)}
  				   	  		/>
  				   	</Form.Group>
  				   		<Form.Group className="mb-3" controlId="formMobileNo">
  				   	  <Form.Label>Mobile No.</Form.Label>
  				   	  <Form.Control 
  				   	  		type="tel" 
  				   	  		placeholder="Enter mobile number"
  				   	  		value ={mobileNo}
  				   	  		onChange ={event => setMobileNo(event.target.value)}
  				   	  		/>
  				   	  	<Form.Text className="text-danger" hidden = {numIsPassed}>
  				   	  		  The mobile number should not exceed 11 digits.
  				   	  	</Form.Text>
  				   	</Form.Group>

  				     <Form.Group className="mb-3" controlId="formBasicEmail">
  				       <Form.Label>Email address</Form.Label>
  				       <Form.Control 
  				       		type="email" 
  				       		placeholder="Enter email"
  				       		value ={email}
  				       		onChange ={event => setEmail(event.target.value)}
  				       		/>
  				       <Form.Text className="text-muted" hidden = {isPassed}>
  				         The email should not exceed 15 characters
  				       </Form.Text>
  				     </Form.Group>

  				     <Form.Group className="mb-3" controlId="formBasicPassword1">
  				       <Form.Label>Password</Form.Label>
  				       <Form.Control 
  				       		type="password" 
  				       		placeholder="Password"
  				       		value = {password1}
  				       		onChange = {event => setPassword1(event.target.value)}
  				       		/>
  				     </Form.Group>

  				      <Form.Group className="mb-3" controlId="formBasicPassword2">
  				       <Form.Label>Confirm Password</Form.Label>
  				       <Form.Control 
  				       		type="password" 
  				       		placeholder="Retype your nominated password"
  				       		value = {password2}
  				       		onChange = {event => setPassword2(event.target.value)}
  				       		/>
  				       	<Form.Text className="text-danger" hidden = {isPasswordMatch}>
  				       		  The passwords does not match!
  				       	</Form.Text>
  				     </Form.Group>

  				     <Button variant="primary" type="submit" disabled = {isDisabled}>
  				       Sign up
  				     </Button>
  				</Form>
  			</Col>
  		</Row>
  		:
 		<Navigate to = '/*' />
	)
}